/* eslint-disable camelcase */
export interface AuthInterface {
  username: string
  password: string
}

export interface UserInterface {
  id?: string
  name?: string
  password?: string
  email?: string
  ktp?: string
  email_verified_at?: Date
  group?: string
  token?: string
  category?: number
  link_zoom?: string
  passcode?: string
  tambahan_waktu?: string
  lolos?: boolean
  meeting_id?: string
  user_soal?: any[]
  nilai_uraian?: any[]
  total_nilai_pg: number
  total_nilai_uraian: number
  total_nilai_akhir: number
  created_at?: Date
  updated_at?: Date
  deleted_at?: Date
}

export interface AuthState {
  token: string
  name: string
  user: UserInterface
}
