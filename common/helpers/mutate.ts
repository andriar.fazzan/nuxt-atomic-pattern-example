export function blurKtp(word: string) {
  return word.substring(0, word.length - 3) + 'XXX'
}

export function dateHuman(data: any) {
  let ds = 'Day, DD/MM/YYYY'

  if (data.waktu_mulai) {
    const dateOri = new Date(data.waktu_mulai.replace(/\s/, 'T'))

    const day = getDayName(data.waktu_mulai.replace(/\s/, 'T'), 'id-ID')
    const date = dateOri.getDate()
    const month = dateOri.getMonth() + 1
    const year = dateOri.getFullYear()

    ds = `${day}, ${date}/${month}/${year}`
  }

  return ds
}

export function checkAllowedStart(data: any) {
  let iA = false
  if (data.waktu_mulai) {
    const dateStart = new Date(data.waktu_mulai.replace(/\s/, 'T'))
    const dateEnd = new Date(data.waktu_selesai.replace(/\s/, 'T'))
    const dateNow = new Date()

    iA = dateNow >= dateStart && dateEnd >= dateNow
  }
  return iA
}

export function getDayName(dateStr: any, locale: string) {
  const date = new Date(dateStr)
  return date.toLocaleDateString(locale, { weekday: 'long' })
}
