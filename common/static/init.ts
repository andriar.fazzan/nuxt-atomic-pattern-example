import { UserInterface } from '@/common/interfaces/auth.interface'

export const initUser: UserInterface = {
  id: '',
  email: '',
  group: '',
  token: '',
  total_nilai_pg: 0,
  total_nilai_uraian: 0,
  total_nilai_akhir: 0,
}

export const initService: any = {
  id: null,
  parent_id: null,
  client_id: null,
  name: null,
  slug: null,
  status: null,
}
