// eslint-disable-next-line nuxt/no-cjs-in-config

let envFileName

// Retrieve the right config file
if (process.env.NODE_ENV === 'production') {
  envFileName = '.env.production'
} else {
  envFileName = '.env'
}

export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'ATOMIC PATTERN',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'ATOMIC PATTERN EXAMPLE',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/css/main.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/axios-accessor' },
    { src: '~/plugins/axios' },
    { src: '~/plugins/currency' },
    { src: '~/plugins/date' },
    { src: '~/plugins/datetime' },
    { src: '~/plugins/vuex-persist', ssr: false },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    [
      '@nuxtjs/dotenv',
      {
        path: './',
        filename: envFileName,
      },
    ],
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.BASE_URL_API,
    proxy: process.env.NODE_ENV !== 'production',
  },

  proxy: {
    '/api/': process.env.BASE_URL_API,
    // '/google/': {
    //   target: 'https://translate.google.com',
    //   pathRewrite: { '^/google/': '' },
    //   changeOrigin: true,
    // },
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  tailwindcss: {
    jit: true,
  },

  loadingIndicator: {
    name: 'cube-grid',
    color: 'white',
    background: '#1cadf2',
  },
}
