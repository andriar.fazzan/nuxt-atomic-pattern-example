import VuexPersistence from 'vuex-persist'

export default ({ store }: any) => {
  new VuexPersistence({
    key: process.env.KEY_LOCAL_STORAGE,
    storage: window.localStorage,
  }).plugin(store)
}
