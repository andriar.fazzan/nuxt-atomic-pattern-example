import Vue from 'vue'
Vue.filter('currency', function (value: any) {
  return value.toLocaleString('id-ID', {
    style: 'currency',
    currency: 'IDR',
    minimumFractionDigits: 0,
  })
})
