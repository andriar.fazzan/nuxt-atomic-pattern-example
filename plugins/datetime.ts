import Vue from 'vue'

Vue.filter('datetime', function (value: any) {
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'Mei',
    'Jun',
    'Jul',
    'Agu',
    'Sep',
    'Okt',
    'Nov',
    'Des',
  ]

  const date = new Date(value)

  const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
  const month = monthNames[date.getMonth() + 1]
  const year = date.getFullYear()
  const minute =
    date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()
  const second =
    date.getSeconds() < 10 ? `0${date.getSeconds()}` : date.getSeconds()

  return `${day} ${month} ${year} | ${minute}:${second}`
})
