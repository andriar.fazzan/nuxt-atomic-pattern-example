import { Plugin } from '@nuxt/types'
import { initializeAxios } from '@/common/utils/axios-instance'

const accessor: Plugin = ({ $axios }) => {
  initializeAxios($axios)
}

export default accessor
