import Vue from 'vue'

Vue.filter('date', function (value: any) {
  const monthNames = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'Mei',
    'Jun',
    'Jul',
    'Agu',
    'Sep',
    'Okt',
    'Nov',
    'Des',
  ]

  const date = new Date(value)

  const day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()
  const month = monthNames[date.getMonth() + 1]
  const year = date.getFullYear()

  return `${day} ${month} ${year}`
})
