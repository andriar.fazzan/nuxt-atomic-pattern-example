import { Store } from 'vuex'
import { initializeStores } from '@/common/utils/store-accessor'

export const strict = false

const initializer = (store: Store<any>) => initializeStores(store)

export const plugins = [initializer]
export * from '@/common/utils/store-accessor'
